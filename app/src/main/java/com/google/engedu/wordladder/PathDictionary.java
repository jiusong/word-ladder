/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.wordladder;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PathDictionary {
    private static final int MAX_WORD_LENGTH = 4;
    private static final int MAX_PATH_LENGTH = 10;
    private static HashSet<String> words = new HashSet<>();
    private static Map<String, GraphNode> graphNodeMap = new HashMap<>();

    public PathDictionary(InputStream inputStream) throws IOException {
        if (inputStream == null) {
            return;
        }
        Log.i("Word ladder", "Loading dict");
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        Log.i("Word ladder", "Loading dict");
        while((line = in.readLine()) != null) {
            String word = line.trim();
            if (word.length() > MAX_WORD_LENGTH) {
                continue;
            }
            words.add(word);
        }
        initGraph(words);
    }

    private void initGraph(Set<String> words) {
        for (String word : words) {
            GraphNode node = graphNodeMap.get(word);
            if (node == null) {
                node = new GraphNode(word);
                graphNodeMap.put(word, node);
            }
            List<String> neighbours = neighbours(node.label);
            for (String neighbor: neighbours) {
                GraphNode neighborNode = graphNodeMap.get(neighbor);
                if (neighborNode == null) {
                    neighborNode = new GraphNode(neighbor);
                    graphNodeMap.put(neighbor, neighborNode);
                }
                node.neighbors.add(neighborNode);
            }
        }
    }

    public boolean isWord(String word) {
        return words.contains(word.toLowerCase());
    }

    private List<String> neighbours(String word) {
        List<String> neighbors = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            for (char c = 'a'; c < 'z'; c++) {
                if (c != word.charAt(i)) {
                    char[] chars = word.toCharArray();
                    chars[i] = c;
                    String str = String.valueOf(chars);
                    if (isWord(str)) {
                        neighbors.add(str);
                    }
                }
            }
        }
        return neighbors;
    }

    public List<String> findPath(String start, String end) {
        List<String> result = new ArrayList<>();
        if (start.length() != end.length()) return result;

        GraphNode startNode = graphNodeMap.get(start);
        List<GraphNode> path = new ArrayList<>();
        path.add(startNode);

        ArrayDeque<List<GraphNode>> deque = new ArrayDeque<>();
        deque.offer(path);

        while (!deque.isEmpty()) {
            List<GraphNode> nodes = deque.poll();
            List<GraphNode> neighbors = nodes.get(nodes.size() -1).neighbors;
            if (neighbors.contains(graphNodeMap.get(end))) {
                // Found the path.
                for (GraphNode node : nodes) {
                    result.add(node.label);
                }
                result.add(end);
                return result;
            } else {
                // Extend path.
                for (GraphNode neighbor : neighbors) {
                    List<GraphNode> newPath = new ArrayList<>();
                    newPath.addAll(nodes);
                    newPath.add(neighbor);
                    deque.offer(newPath);
                }
            }
            if (nodes.size() > MAX_PATH_LENGTH) break;
        }
        return result;
    }
}
