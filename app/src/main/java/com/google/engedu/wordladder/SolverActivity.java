package com.google.engedu.wordladder;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.engedu.worldladder.R;
import com.google.engedu.worldladder.databinding.ActivityDisplaySolverBinding;

import java.util.ArrayList;
import java.util.List;

public class SolverActivity extends Activity {

    private List<String> words;
    private List<String> results;
    private ActivityDisplaySolverBinding binding;
    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_display_solver);
        Intent intent = getIntent();
        words = intent.getStringArrayListExtra(Constants.INTENT_KEY_PATH);
        initUI();
    }

    private void initUI() {
        if (words == null || words.size() < 2) return;
        binding.startTextView.setText(words.get(0));
        binding.endTextView.setText(words.get(words.size() -1));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        words.remove(0);
        words.remove(words.size() -1);
        results = new ArrayList<>(words.size());
        for (int i = 0; i < words.size(); i++) {
            results.add("");
        }

        listAdapter = new ListAdapter(results);
        binding.recyclerView.setAdapter(listAdapter);
    }


    public void onSolve(View view) {
        listAdapter = new ListAdapter(words);
        binding.recyclerView.setAdapter(listAdapter);
    }

}
