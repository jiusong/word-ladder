package com.google.engedu.wordladder;

import java.util.ArrayList;
import java.util.List;

public class GraphNode {
    String label;
    List<GraphNode> neighbors;

    public GraphNode(String label) {
        this.label = label;
        neighbors = new ArrayList<>();
    }
}
